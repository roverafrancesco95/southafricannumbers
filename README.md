# SouthAfricanPhoneNumber - FR

Il progetto è stato realizzato e suddiviso in 3 componenti principali:
* 4 API per l'interazione con l'utente (upload file, richiesta risulati per file, richiesta risulati paginati, validazione singolo numero)
* Loader: un metodo schedulato che si occupa della lettura del file caricato tramite API e del suo salvataggio su DB
* Validator: un metodo schedulato che si occupa della validazione di ciascun numero di telefono

> **Nota** Questa architettura è stata progettata per poter far fronte ad un elevato carico di file ricevuti in ingresso. 
> Di fatto aggiungendo una parte di gestione della concorrenza sarebbe possibile far girare l'applicazione su nodi multipli per avere un'elevato throughput di numeri processati.

> **Nota** Si è deciso di procedere alla fase di validazione di un file dopo che tutti i numeri presenti all'interno di esso fossero stati caricati su DB. 
> Per ottimizzare ulteriormente il processo si potrebbe iniziare a processare i numeri non appena vengono salvati sul DB senza attendere il completamento del caricamento del file. 

## Avvio del progetto
Basterà aprire il progetto con un IDE che supporti Springboot ed eseguire la classe _SouthAfricanNumbersApplication_.

Sarà necessario creare 3 cartelle all'interno della root del progetto (o comunque modificare l'application.yaml sezione 'dir' in modo che punti a 3 directory esistenti e vuote):
* data/dump
* data/processed
* data/tmp

# Database - H2

Per svolgere questo progetto è stato utilizzato un DB relazione in memory (H2).
La console per accedere al DB utilizzato è accessibile al seguente URL:

    http://localhost:8080/h2-console/login.jsp 

I parametri di connessione sono:
* JDBC URL = jdbc:h2:mem:interlogica 
* user = sa
* no password

> **Nota** All'avvio le tabelle del DB saranno vuote, non è previsto alcun seed file per l'inserimento dati. 

## Tabelle
Le tabelle create sono 3: FILES_UPLOADED, DISCARDED_ROWS e PHONE_NUMBERS

### FILES_UPLOADED
Contiene la lista dei file caricati ed è composta dalle seguenti colonne:
* **ID**: identificativo univoco del file (PK)
* **TIMESTAMP**: data di caricamento
* **IS_VALID**: flag boolean che indica la correttezza della struttura del file caricato
* **STATUS**: indica lo stato del file, come indicato nell'enum FileUploadedStatus, può assumere i seguenti valori:
    * 0 - LOADING: caricamento in corso
    * 1 - VALIDABLE: pronto per la validazione
    * 2 - VALIDATED: terminata la validazione di tutte le sue righe
    * 3 - ERROR

### DISCARDED_ROWS
Tabella che contiene tutte le righe di un file che sono andate in errore durante il processo di parsing. Le sue colonne sono:
* **ID**: identificativo univoco della riga in errore (PK)
* **ROW_VALUE**: stringa della riga in errore
* **CREATION_TIMESTAMP**
* **FILE_UPLOADED_ID**: indica il file di appartenenza della riga in questione (FK verso FILES_UPLOADED)

### PHONE_NUMBERS
Tabella che contiene tutti i numeri di telefono di ciascun file. E' composta dai seguenti campi:
* **ID**: identificativo univoco della riga (PK)
* **PHONE_ID**: corrisponde alla prima colonna del csv
* **PHONE_NUMBER**: corrisponde alla seconda colonna del csv
* **PHONE_NUMBER_CORRECTION**: eventuale correzione del numero di telefono
* **DESCRIPTION**: eventuale descrizione del processo di validazione del numero di telefono
* **CREATION_TIMESTAMP**
* **FILE_UPLOADED_ID**: indica il file di appartenenza del numero di telefono in questione (FK verso FILES_UPLOADED)
* **STATUS**: indica lo stato del numero di telefono. Può assumere i seguenti valori:
Gli stati di un numero (come indicato nell'enum PhoneNumberStatus) possono essere:
    * 0 LOADED
    * 1 VALID
    * 2 CORRECTED
    * 3 INVALID
    * 4 ERROR

# API
Per questo progetto, in base alle specifiche, si è deciso di sviluppare 4 API:
* **[POST] /api/uploadFile** - Permette di caricare un file csv contenente i numeri da salvare ed eventualmente correggere. Ritornerà all'utente un ID univoco corrispondente al file appena caricato;
* **[GET] /api/getResults** - Dato un ID di un file precedentemente caricato permette di recuperare tutti i numeri di telefono ad esso associati con i dettagli relativi la validità dei numeri, eventuali correzioni e una descrizione;
* **[GET] /api/getResultsPageable** - Ritorna tutti i numeri paginati con relativi stati e correzioni (richiede page e size come parametri nella request);
* **[GET] /api/correctSinglePhoneNumber** - Specificando un numero di telefono come parametro nella request ritornerà le informazioni inerenti la sua validità e, nel caso di inesattezze, si provvederà alla correzione se possibile (in questo caso il numero non verrà persistito su DB).

## Swagger

Al seguente URL è possibile consultare lo swagger del progetto che ne riassume tutte le API:

    http://localhost:8080/swagger-ui/index.html

## Collection Postman 

Nella root del progetto troverete anche il seguente file contenente la collection Postman per effettuare le 4 chiamate alle API (da swagger l'upload del file non è supportato)

    SouthAfricanNumbersApplication.postman_collection.json

# Scheduled methods
Come anticipato sono presenti due metodi schedulati (per semplicità sono eseguiti entrambi sullo stesso thread) .

## Loader
Una volta caricato un file tramite apposita API esso verrà salvato in una directory nel progetto (vedi application.yaml).
Il componente 'Loader' ad ogni iterazione verificherà se all'intento di tale dir sia presente un file da processare.
In caso affermativo verrà inserita la relativa riga nella tabella FILE_UPDATED e successivamente verranno inseriti i numeri di telefono contenuti all'interno della tabella PHONE_NUMBERS.

Nel caso in cui il file non dovesse essere conforme alle specifiche verrà settato come non valido (la logica del metodo che ne verifica la validità è da implementare, per ora è un semplice 'return true').

Se dovessero verificarsi errori di parsing delle singole righe quest'ultime verranno inserite in una tabella appostia (DISCARDED_ROWS).

> **Nota** Sempre in un'ottica di ottimizzazione, per evitare di sovraccaricare il DB, nelle properties è presente un parametro 'loader.batch_limit' che indica la dimensione massima del batch di righe che possono essere inserite sul db in una operazione. 

## Validator
Una volta che il componente 'Loder' ha caricato su DB tutte le righe del file lo stato di quest'ultimo verrà impostato a **VALIDABLE** in modo che possa essere processato dal _Validator_.
Il validator si occupa di recuperare il file in attesa da più tempo pronto per la validazione e procedrà con il recuperarne tutte le righe procedendo a batch della dimensione indicata nella seguente proprerty:

    validator.max-fetchable-rows: 150

Iterando su tutti i numeri di telefono si procederà quindi con la validazione per determinarne lo stato ed eventualmente procedere con la correzione.


### Criteri validazione
Si assume che i numeri Sudafricani siano composti come segue:

    27XXYYYYYYY

Dove:
* 27 indica il prefisso internazionale
* XX indica il prefisso. Al segunte link https://www.easyexpat.com/it/guides/sud-africa/johannesburg/trasferirsi/telefono.htm si sono recuperati una lista di prefissi ritenuti validi. Tale lista di prefissi può essere modificata dall'application.yaml

 
    validation.accepted-prefix: "(00|01|10|11|08|80|81|82|83|84|85|86|87|88|89)"

* YYYYYYY sono le 7 cifre del numero (che non hanno quindi vincoli di validità)

Sono considerati validi (status=**VALID**) i numeri composti da:
* **27 + uno dei prefissi della lista + 7 digits**
* **27 + 2 digits + 7 digits** (Non sapendo se la lista recuperata sia corretta si è deciso di validare anche questi numeri, nella descrizione avranno la seguente stringa 'WARN: PREFIX_NOT_RECOGNIZED')

Sono stati corretti corretti (status=**CORRECTED**) i numeri composti da:
* **uno dei prefissi della lista + 7 digits** (descr: 'MISSING_INTERNATIONAL_PREFIX')
* **2 digits + 7 digits** (descr: 'WARN: MISSING_INTERNATIONAL_PREFIX and PREFIX_NOT_RECOGNIZED')
* le stringhe che al loro interno contengono '_DELETED_' in una qualsiasi posizione e hanno **27 + uno dei prefissi della lista + 7 digits** (descr: 'FOUND_CORRECT_NUMBER_IN_WRONG_STRING')
* le stringhe che al loro interno contengono '_DELETED_' in una qualsiasi posizione e hanno **27 + 2 digits + 7 digits** (descr: 'WARN: FOUND_CORRECT_NUMBER_IN_WRONG_STRING and PREFIX_NOT_RECOGNIZED')

Sono stati considerati invalidi (status=**INVALID**) tutti gli altri numeri.

In caso di errori durante la validazione al numero telefonico verrà assegnato lo stato di errore (status=**ERROR**).






# Unit test
La parte di test si è concentrata sulla validazione/correzione del numero di telefono.

