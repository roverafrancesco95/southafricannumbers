package com.fr.southafricannumbers;

import com.fr.southafricannumbers.api.models.SinglePhoneNumberResponse;
import com.fr.southafricannumbers.database.enums.PhoneNumberStatusEnum;
import com.fr.southafricannumbers.services.PhoneNumberService;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@RequiredArgsConstructor
class SouthAfricanPhoneNumberApplicationTests {

    @Autowired
    private PhoneNumberService phoneNumberService;

    @Test
    void validateNull() {
        assertThrows(Exception.class, () -> phoneNumberService.validate(null));
    }

    @Test
    void validateEmpty() {
        assertThrows(Exception.class, () -> phoneNumberService.validate("   "));
    }

    @Test
    void validateInvalid1() throws Exception {
        SinglePhoneNumberResponse resp = phoneNumberService.validate("1234");
        assertEquals(PhoneNumberStatusEnum.INVALID.getValue(), resp.getStatus());
    }

    @Test
    void validateInvalid2() throws Exception {
        SinglePhoneNumberResponse resp = phoneNumberService.validate("2783123456");
        assertEquals(PhoneNumberStatusEnum.INVALID.getValue(), resp.getStatus());
    }

    @Test
    void validateInvalid3() throws Exception {
        SinglePhoneNumberResponse resp = phoneNumberService.validate("2783123456789");
        assertEquals(PhoneNumberStatusEnum.INVALID.getValue(), resp.getStatus());
    }

    @Test
    void validateInvalid4() throws Exception {
        SinglePhoneNumberResponse resp = phoneNumberService.validate("27 83 1234567");
        assertEquals(PhoneNumberStatusEnum.INVALID.getValue(), resp.getStatus());
    }

    @Test
    void validateInvalid5() throws Exception {
        SinglePhoneNumberResponse resp = phoneNumberService.validate("+27831234567");
        assertEquals(PhoneNumberStatusEnum.INVALID.getValue(), resp.getStatus());
    }

    @Test
    void validateInvalid6() throws Exception {
        SinglePhoneNumberResponse resp = phoneNumberService.validate("27831_DELETED_234567");
        assertEquals(PhoneNumberStatusEnum.INVALID.getValue(), resp.getStatus());
    }

    @Test
    void validateValid1() throws Exception {
        SinglePhoneNumberResponse resp = phoneNumberService.validate("27831234567");
        assertNull(resp.getCorrectionDescription());
        assertEquals(PhoneNumberStatusEnum.VALID.getValue(), resp.getStatus());
    }

    @Test
    void validateValid2() throws Exception {
        SinglePhoneNumberResponse resp = phoneNumberService.validate("27001234567");
        assertNull(resp.getCorrectionDescription());
        assertEquals(PhoneNumberStatusEnum.VALID.getValue(), resp.getStatus());
    }

    @Test
    void validateValid3() throws Exception {
        SinglePhoneNumberResponse resp = phoneNumberService.validate("27005456789");
        assertNull(resp.getCorrectionDescription());
        assertEquals(PhoneNumberStatusEnum.VALID.getValue(), resp.getStatus());
    }

    @Test
    void validateValid4() throws Exception {
        SinglePhoneNumberResponse resp = phoneNumberService.validate("27995456789");
        assertNotNull(resp.getCorrectionDescription());
        assertEquals(PhoneNumberStatusEnum.VALID.getValue(), resp.getStatus());
    }

    @Test
    void validateCorrected1() throws Exception {
        SinglePhoneNumberResponse resp = phoneNumberService.validate("835456789");
        assertEquals("MISSING_INTERNATIONAL_PREFIX", resp.getCorrectionDescription());
        assertEquals("27835456789", resp.getCorrectedPhoneNumber());
        assertEquals(PhoneNumberStatusEnum.CORRECTED.getValue(), resp.getStatus());
    }

    @Test
    void validateCorrected2() throws Exception {
        SinglePhoneNumberResponse resp = phoneNumberService.validate("995456789");
        assertEquals("WARN: MISSING_INTERNATIONAL_PREFIX and PREFIX_NOT_RECOGNIZED", resp.getCorrectionDescription());
        assertEquals("27995456789", resp.getCorrectedPhoneNumber());
        assertEquals(PhoneNumberStatusEnum.CORRECTED.getValue(), resp.getStatus());
    }

    @Test
    void validateCorrected3() throws Exception {
        SinglePhoneNumberResponse resp = phoneNumberService.validate("27835456789_DELETED_");
        assertEquals("FOUND_CORRECT_NUMBER_IN_WRONG_STRING", resp.getCorrectionDescription());
        assertEquals("27835456789", resp.getCorrectedPhoneNumber());
        assertEquals(PhoneNumberStatusEnum.CORRECTED.getValue(), resp.getStatus());
    }

    @Test
    void validateCorrected4() throws Exception {
        SinglePhoneNumberResponse resp = phoneNumberService.validate("27995456789_DELETED_");
        assertEquals("WARN: FOUND_CORRECT_NUMBER_IN_WRONG_STRING and PREFIX_NOT_RECOGNIZED", resp.getCorrectionDescription());
        assertEquals("27995456789", resp.getCorrectedPhoneNumber());
        assertEquals(PhoneNumberStatusEnum.CORRECTED.getValue(), resp.getStatus());
    }
}
