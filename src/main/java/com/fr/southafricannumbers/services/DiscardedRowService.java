package com.fr.southafricannumbers.services;

import com.fr.southafricannumbers.database.entities.DiscardedRow;
import com.fr.southafricannumbers.database.repositories.DiscardedRowRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class DiscardedRowService {
    private final DiscardedRowRepository repository;

    public Iterable<DiscardedRow> saveAll(List<DiscardedRow> phones) {
        return repository.saveAll(phones);
    }
}
