package com.fr.southafricannumbers.services;

import com.fr.southafricannumbers.database.entities.FileUploaded;
import com.fr.southafricannumbers.database.enums.FileUploadedStatus;
import com.fr.southafricannumbers.database.repositories.FileUploadedRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class FileUploadedService {

    private final FileUploadedRepository repository;

    public FileUploaded save(FileUploaded fileUploaded) {
        return repository.save(fileUploaded);
    }

    public Optional<FileUploaded> findById(String id) {
        return repository.findById(id);
    }

    public Optional<FileUploaded> findFileToBeValidated() {
        return repository.findFirstByStatusAndIsValidOrderByCreationTimestampAsc(FileUploadedStatus.VALIDABLE.getValue(), true);
    }
}
