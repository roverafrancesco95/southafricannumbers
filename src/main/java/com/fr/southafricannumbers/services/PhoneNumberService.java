package com.fr.southafricannumbers.services;

import com.fr.southafricannumbers.api.models.SinglePhoneNumberResponse;
import com.fr.southafricannumbers.database.entities.FileUploaded;
import com.fr.southafricannumbers.database.entities.PhoneNumber;
import com.fr.southafricannumbers.database.enums.PhoneNumberStatusEnum;
import com.fr.southafricannumbers.database.repositories.PhoneNumberRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Service
@RequiredArgsConstructor
public class PhoneNumberService {

    @Value("${validator.accepted-prefix}")
    private String PREFIX_LIST;

    private final PhoneNumberRepository repository;

    public void saveAll(List<PhoneNumber> phones) {
        repository.saveAll(phones);
    }

    public List<PhoneNumber> getPhoneNumbersByFileUploaded(FileUploaded fileUploaded) {
        return repository.findByFileUploaded(fileUploaded);
    }

    public List<PhoneNumber> getPhoneNumbersValidatedPageable(int page, int size) {
        PageRequest pageable = PageRequest.of(page, size, Sort.by("id").ascending());
        return repository.findByStatusGreaterThan(PhoneNumberStatusEnum.LOADED.getValue(), pageable);
    }

    public long getPagesCountByFileUploaded(FileUploaded fileUploaded, int maxFetchableRows) {
        long rowsCount = repository.countByFileUploadedAndStatus(fileUploaded, PhoneNumberStatusEnum.LOADED.getValue());
        long pageCount = (long) Math.ceil(rowsCount / (double) maxFetchableRows);
        log.info(String.format("File id [%s], status [%s] rows count [%d], maxFetchableRows [%d] -> %d pages",
                fileUploaded.getId(), fileUploaded.getStatus(), rowsCount, maxFetchableRows, pageCount));
        return pageCount;
    }

    public List<PhoneNumber> getPhoneNumbersReadyToBeValidated(FileUploaded fileUploaded, PageRequest pageable) {
        return repository.findByFileUploadedAndStatusOrderByIdAsc(fileUploaded, PhoneNumberStatusEnum.LOADED.getValue(), pageable);
    }

    public SinglePhoneNumberResponse validate(String phoneNumber) throws Exception {
        SinglePhoneNumberResponse resp = new SinglePhoneNumberResponse(phoneNumber);

        // https://www.easyexpat.com/it/guides/sud-africa/johannesburg/trasferirsi/telefono.htm
        // 27 83 1234567

        final String CORRECT_NUMBER_REGEX = String.format("^27%s\\d{7}$", PREFIX_LIST);
        final String CORRECT_NUMBER_NO_PREFIX_RECOGNIZED_REGEX = "^27\\d{9}$";
        final String NO_PREFIX_REGEX = String.format("^%s\\d{7}$", PREFIX_LIST);
        final String NO_PREFIX_NO_PREFIX_RECOGNIZED_REGEX = "^\\d{9}$";
        final String PARTIAL_CORRECT_NUMBER_REGEX = String.format("27%s\\d{7}", PREFIX_LIST);
        final String PARTIAL_CORRECT_NUMBER_NO_PREFIX_RECOGNIZED_REGEX = "27\\d{9}";


        if (!StringUtils.hasText(phoneNumber)) {
            throw new Exception("Phone number is null or empty");
        }


        if (matches(CORRECT_NUMBER_REGEX, phoneNumber)) {
            resp.setStatus(PhoneNumberStatusEnum.VALID.getValue());
            return resp;
        }

        if (matches(CORRECT_NUMBER_NO_PREFIX_RECOGNIZED_REGEX, phoneNumber)) {
            resp.setStatus(PhoneNumberStatusEnum.VALID.getValue());
            resp.setCorrectionDescription("WARN: PREFIX_NOT_RECOGNIZED");
            return resp;
        }

        if (matches(NO_PREFIX_REGEX, phoneNumber)) {
            resp.setStatus(PhoneNumberStatusEnum.CORRECTED.getValue());
            resp.setCorrectedPhoneNumber(String.format("27%s", phoneNumber));
            resp.setCorrectionDescription("MISSING_INTERNATIONAL_PREFIX");
            return resp;
        }

        if (matches(NO_PREFIX_NO_PREFIX_RECOGNIZED_REGEX, phoneNumber)) {
            resp.setStatus(PhoneNumberStatusEnum.CORRECTED.getValue());
            resp.setCorrectedPhoneNumber(String.format("27%s", phoneNumber));
            resp.setCorrectionDescription("WARN: MISSING_INTERNATIONAL_PREFIX and PREFIX_NOT_RECOGNIZED");
            return resp;
        }


        if(phoneNumber.contains("_DELETED_")) {
            String potentialNumber = findGroup(PARTIAL_CORRECT_NUMBER_REGEX, phoneNumber);
            if (StringUtils.hasText(potentialNumber)) {
                resp.setStatus(PhoneNumberStatusEnum.CORRECTED.getValue());
                resp.setCorrectedPhoneNumber(potentialNumber);
                resp.setCorrectionDescription("FOUND_CORRECT_NUMBER_IN_WRONG_STRING");
                return resp;
            }

            potentialNumber = findGroup(PARTIAL_CORRECT_NUMBER_NO_PREFIX_RECOGNIZED_REGEX, phoneNumber);
            if (StringUtils.hasText(potentialNumber)) {
                resp.setStatus(PhoneNumberStatusEnum.CORRECTED.getValue());
                resp.setCorrectedPhoneNumber(potentialNumber);
                resp.setCorrectionDescription("WARN: FOUND_CORRECT_NUMBER_IN_WRONG_STRING and PREFIX_NOT_RECOGNIZED");
                return resp;
            }
        }

        resp.setStatus(PhoneNumberStatusEnum.INVALID.getValue());
        return resp;
    }


    private boolean matches(String regex, String phoneNumber) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(phoneNumber);
        return matcher.matches();
    }

    private String findGroup(String regex, String phoneNumber) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(phoneNumber);
        if (matcher.find())
            return matcher.group();
        else
            return null;
    }
}
