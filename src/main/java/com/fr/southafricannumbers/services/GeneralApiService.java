package com.fr.southafricannumbers.services;

import com.fr.southafricannumbers.api.models.GetResultsResponse;
import com.fr.southafricannumbers.api.models.SinglePhoneNumberResponse;
import com.fr.southafricannumbers.api.models.UploadFileResponse;
import com.fr.southafricannumbers.database.entities.FileUploaded;
import com.fr.southafricannumbers.database.entities.PhoneNumber;
import com.fr.southafricannumbers.database.enums.FileUploadedStatus;
import com.fr.southafricannumbers.database.enums.PhoneNumberStatusEnum;
import com.fr.southafricannumbers.exceptions.ApiException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class GeneralApiService {

    @Value("${dir.tmp}")
    private String tmpDir;

    private final FileUploadedService fileUploadedService;
    private final PhoneNumberService phoneNumberService;

    public UploadFileResponse uploadFile(MultipartFile requestFile) {
        if (requestFile.isEmpty()) {
            throw new ApiException(HttpStatus.BAD_REQUEST, "ERR400_EMPTY_FILE", "Failed to store empty file.");
        }

        String fileId = String.valueOf(UUID.randomUUID());
        Path filePath = Paths.get(tmpDir + "/" + fileId);

        File file = new File(filePath.toUri());
        if (file.exists())
            throw new ApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERR500_FILE",
                    "Trying to overwrite another file, please retry");

        log.info(String.format("Saving file with ID [%s]", fileId));
        try (InputStream inputStream = requestFile.getInputStream()) {
            Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            log.error("Error on copying file", e);
            throw new ApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERR500_COPY_FILE", "Error on copying file");
        }
        log.info(String.format("File with ID [%s] saved", fileId));
        return new UploadFileResponse(fileId);
    }

    public GetResultsResponse getResults(String fileId) {
        if (!StringUtils.hasText(fileId)) {
            throw new ApiException(HttpStatus.BAD_REQUEST, "ERR400_EMPTY_FILE_ID",
                    "File ID in request is null or empty");
        }

        Optional<FileUploaded> fileOpt = fileUploadedService.findById(fileId);
        if (!fileOpt.isPresent())
            throw new ApiException(HttpStatus.NOT_FOUND, "ERR404_NO_FILE_FOUND",
                    String.format("File with specified ID [%s] doesn't exist", fileId));

        FileUploaded file = fileOpt.get();
        if(!file.isValid())
            throw new ApiException(HttpStatus.BAD_REQUEST, "ERR400_FILE_NOT_VALID",
                    String.format("File with specified ID [%s] is not valid and so unable to be processed", fileId));
        if (file.getStatus() == FileUploadedStatus.ERROR.getValue()) {
            throw new ApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERR404_FILE_ON_ERROR",
                    String.format("File with specified ID [%s] is unable to be processed", fileId));
        } else if (file.getStatus() != FileUploadedStatus.VALIDATED.getValue()) {
            throw new ApiException(HttpStatus.NOT_FOUND, "ERR404_NOT_READY",
                    String.format("File with specified ID [%s] has not been processed yet, please try again in a few seconds", fileId));
        }

        List<PhoneNumber> numbers = phoneNumberService.getPhoneNumbersByFileUploaded(file);
        log.info(String.format("Found [%d] numbers for file with ID [%s]", numbers.size(), fileId));
        if (numbers.isEmpty())
            throw new ApiException(HttpStatus.NOT_FOUND, "ERR404_NO_NUMBERS",
                    String.format("File with specified ID [%s] has no numbers", fileId));

        return new GetResultsResponse(fileId, numbers);

    }

    public GetResultsResponse getResults(int page, int size) {
        if(page < 0 || size <= 0) {
            throw new ApiException(HttpStatus.BAD_REQUEST, "ERR400_BAD_REQ_PARAMS",
                    "Page/size in request are negative");
        }

        List<PhoneNumber> numbers = phoneNumberService.getPhoneNumbersValidatedPageable(page, size);
        if (numbers.isEmpty())
            throw new ApiException(HttpStatus.NOT_FOUND, "ERR404_NO_NUMBERS",
                    "No numbers present yet");
        log.info(String.format("Found [%d] numbers ", numbers.size()));

        return new GetResultsResponse("ALL_FILES", numbers);

    }

    public SinglePhoneNumberResponse correctSinglePhoneNumber(String phoneNumber) {
        if (!StringUtils.hasText(phoneNumber)) {
            throw new ApiException(HttpStatus.BAD_REQUEST, "ERR400_EMPTY_PHONE_NUMBER",
                    "Phone number in request is null or empty");
        }

        try {
            SinglePhoneNumberResponse resp = phoneNumberService.validate(phoneNumber);
            resp.setStatusDescr(PhoneNumberStatusEnum.fromValue(resp.getStatus()).toString());
            return resp;
        } catch (Exception e) {
            throw new ApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERR500_VALIDATION",
                    String.format("Unable to validate current phone number [%s]. ErrorMsg: [%s]", phoneNumber, e.getMessage()));
        }
    }
}
