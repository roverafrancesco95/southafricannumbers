package com.fr.southafricannumbers.api.controllers;

import com.fr.southafricannumbers.api.models.ErrorResponse;
import com.fr.southafricannumbers.api.models.GetResultsResponse;
import com.fr.southafricannumbers.api.models.SinglePhoneNumberResponse;
import com.fr.southafricannumbers.api.models.UploadFileResponse;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

public interface GeneralApi {
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Ok"),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class)))})
    @PostMapping(
            value = {"/uploadFile"},
            produces = {"application/json"}
    )
    ResponseEntity<UploadFileResponse> uploadFile(@RequestParam("file") MultipartFile file);

    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Ok"),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class)))})
    @GetMapping(
            value = {"/getResults"},
            produces = {"application/json"}
    )
    ResponseEntity<GetResultsResponse> getResults(@RequestParam("fileUUID") String fileUUID);

    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Ok"),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class)))})
    @GetMapping(
            value = {"/getResultsPageable"},
            produces = {"application/json"}
    )
    ResponseEntity<GetResultsResponse> getResultsPageable(@RequestParam("page") int page,
                                                  @RequestParam("size") int size);

    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Ok"),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class)))})
    @GetMapping(
            value = {"/correctSinglePhoneNumber"},
            produces = {"application/json"}
    )
    ResponseEntity<SinglePhoneNumberResponse> correctSinglePhoneNumber(@RequestParam("phoneNumber") String phoneNumber);
}
