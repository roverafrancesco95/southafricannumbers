package com.fr.southafricannumbers.api.controllers;

import com.fr.southafricannumbers.api.models.GetResultsResponse;
import com.fr.southafricannumbers.api.models.SinglePhoneNumberResponse;
import com.fr.southafricannumbers.api.models.UploadFileResponse;
import com.fr.southafricannumbers.services.GeneralApiService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class GeneralApiController implements GeneralApi {
    private final GeneralApiService service;

    @Override
    public ResponseEntity<UploadFileResponse> uploadFile(MultipartFile file) {
        log.info("Received file to be uploaded request");
        UploadFileResponse response = service.uploadFile(file);
        log.info("File stored");
        return ResponseEntity.ok().body(response);
    }

    @Override
    public ResponseEntity<GetResultsResponse> getResults(String fileUUID) {
        log.info(String.format("Received getResults request for file with UUID [%s]", fileUUID));
        GetResultsResponse response = service.getResults(fileUUID);
        log.info("Results sent");
        return ResponseEntity.ok().body(response);
    }

    @Override
    public ResponseEntity<GetResultsResponse> getResultsPageable(int page, int size) {
        log.info(String.format("Received getResults request for file with page [%d] and size [%d]", page, size));
        GetResultsResponse response = service.getResults(page, size);
        log.info("Results sent");
        return ResponseEntity.ok().body(response);
    }

    @Override
    public ResponseEntity<SinglePhoneNumberResponse> correctSinglePhoneNumber(String phoneNumber) {
        log.info(String.format("Received correctSinglePhoneNumber request for phone number [%s]", phoneNumber));
        SinglePhoneNumberResponse response = service.correctSinglePhoneNumber(phoneNumber);
        log.info("phoneNumber ended");
        return ResponseEntity.ok().body(response);
    }
}
