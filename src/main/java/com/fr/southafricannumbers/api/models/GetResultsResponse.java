package com.fr.southafricannumbers.api.models;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.fr.southafricannumbers.database.entities.PhoneNumber;
import com.fr.southafricannumbers.database.enums.PhoneNumberStatusEnum;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;

import java.io.IOException;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Slf4j
public class GetResultsResponse {
    private String fileId;
    private String timestamp;
    private List<PhoneNumberResponse> phoneNumbers;

    public GetResultsResponse(String fileId, List<PhoneNumber> phoneNumbers) {
        ModelMapper modelMapper = new ModelMapper();
        this.fileId = fileId;
        this.phoneNumbers = phoneNumbers.stream()
                .map(x -> modelMapper.map(x, PhoneNumberResponse.class)).collect(Collectors.toList());
        this.phoneNumbers.forEach(x -> x.setStatusDescr(PhoneNumberStatusEnum.fromValue(x.getStatus()).toString()));
        this.timestamp = String.valueOf(Instant.now().toEpochMilli());
    }
}
