package com.fr.southafricannumbers.api.models;

import lombok.Data;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.sql.Timestamp;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PhoneNumberResponse {
    private Integer id;
    private String phoneId;
    private String phoneNumber;
    private int status;
    private String statusDescr;
    private String phoneNumberCorrection;
    private String description;
    private Timestamp creationTimestamp;

}
