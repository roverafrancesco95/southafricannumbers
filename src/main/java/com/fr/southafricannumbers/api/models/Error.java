package com.fr.southafricannumbers.api.models;

import lombok.Data;

@Data
public class Error {
    private String code;
    private String description;
}
