package com.fr.southafricannumbers.api.models;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class UploadFileResponse {
    private final String fileId;
}
