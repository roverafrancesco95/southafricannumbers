package com.fr.southafricannumbers.api.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SinglePhoneNumberResponse {

    private final String originalPhoneNumber;
    private int status;
    private String statusDescr;
    private String correctedPhoneNumber;
    private String correctionDescription;

}
