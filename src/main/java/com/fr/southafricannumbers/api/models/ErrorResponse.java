package com.fr.southafricannumbers.api.models;

import lombok.Data;

import java.util.List;

@Data
public class ErrorResponse {
    private List<Error> errors;
    private String timestamp;
}
