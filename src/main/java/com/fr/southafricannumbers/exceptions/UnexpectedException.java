package com.fr.southafricannumbers.exceptions;

import com.fr.southafricannumbers.exceptions.ApiException;
import org.springframework.http.HttpStatus;

public class UnexpectedException extends ApiException {

    public UnexpectedException() {
        super(HttpStatus.INTERNAL_SERVER_ERROR, "ER00000", "Unexpected error");
    }

    public UnexpectedException(String message) {
        super(message, HttpStatus.INTERNAL_SERVER_ERROR, "ER00000", "Unexpected error");
    }

    public UnexpectedException(String message, Throwable cause) {
        super(message, cause, HttpStatus.INTERNAL_SERVER_ERROR, "ER00000", "Unexpected error");
    }

    public UnexpectedException(Throwable cause) {
        super(cause, HttpStatus.INTERNAL_SERVER_ERROR, "ER00000", "Unexpected error");
    }

    public UnexpectedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace, HttpStatus.INTERNAL_SERVER_ERROR, "ER00000",
                "Unexpected error");
    }
}
