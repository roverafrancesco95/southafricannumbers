package com.fr.southafricannumbers.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class ApiException extends RuntimeException {

    private final HttpStatus httpStatus;
    private final String code;
    private final String description;

    public ApiException(HttpStatus httpStatus, String code, String description) {
        super(description);
        this.httpStatus = httpStatus;
        this.code = code;
        this.description = description;
    }

    public ApiException(String message, HttpStatus httpStatus, String code, String description) {
        super(message);
        this.httpStatus = httpStatus;
        this.code = code;
        this.description = description;
    }

    public ApiException(String message, Throwable cause, HttpStatus httpStatus, String code, String description) {
        super(message, cause);
        this.httpStatus = httpStatus;
        this.code = code;
        this.description = description;
    }

    public ApiException(Throwable cause, HttpStatus httpStatus, String code, String description) {
        super(cause);
        this.httpStatus = httpStatus;
        this.code = code;
        this.description = description;
    }

    public ApiException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace,
                        HttpStatus httpStatus, String code, String description) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.httpStatus = httpStatus;
        this.code = code;
        this.description = description;
    }
}
