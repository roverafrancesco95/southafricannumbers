package com.fr.southafricannumbers.exceptions;

import org.springframework.http.HttpStatus;

public class InvalidRequestException extends ApiException {
    public InvalidRequestException(String description) {
        super(HttpStatus.BAD_REQUEST, "ER00400", description);
    }

    public InvalidRequestException(String message, String description) {
        super(message, HttpStatus.BAD_REQUEST, "ER00400", description);
    }

    public InvalidRequestException(String message, Throwable cause, String description) {
        super(message, cause, HttpStatus.BAD_REQUEST, "ER00400", description);
    }

    public InvalidRequestException(Throwable cause, String description) {
        super(cause, HttpStatus.BAD_REQUEST, "ER00400", description);
    }

    public InvalidRequestException(String message, Throwable cause, boolean enableSuppression,
                                   boolean writableStackTrace, String description) {
        super(message, cause, enableSuppression, writableStackTrace, HttpStatus.BAD_REQUEST, "ER00400", description);
    }
}
