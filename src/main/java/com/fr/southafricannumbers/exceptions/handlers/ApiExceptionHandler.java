package com.fr.southafricannumbers.exceptions.handlers;

import com.fr.southafricannumbers.api.models.Error;
import com.fr.southafricannumbers.api.models.ErrorResponse;
import com.fr.southafricannumbers.exceptions.ApiException;
import com.fr.southafricannumbers.exceptions.InvalidRequestException;
import com.fr.southafricannumbers.exceptions.UnexpectedException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.util.WebUtils;

import java.time.Instant;
import java.util.Collections;

@Slf4j
@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
                                                             HttpStatus status, WebRequest request) {

        if (HttpStatus.BAD_REQUEST.equals(status)) {
            return apiException(new InvalidRequestException(ex, ex.getMessage()));
        } else {
            request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex, WebRequest.SCOPE_REQUEST); // ??
            return unexpectedException(ex);
        }
    }

    @ExceptionHandler({ApiException.class})
    protected ResponseEntity<Object> apiException(ApiException ex) {
        log.error("{} - {}: {}", ex.getHttpStatus(), ex.getCode(), ex.getDescription(), ex);
        ErrorResponse errorResponse = createErrorResponse(ex);
        return ResponseEntity.status(ex.getHttpStatus()).body(errorResponse);
    }

    @ExceptionHandler({Exception.class})
    protected ResponseEntity<Object> unexpectedException(Exception ex) {
        return apiException(new UnexpectedException(ex));
    }

    private ErrorResponse createErrorResponse(ApiException apiException) {
        Error error = new Error();
        error.setCode(apiException.getCode());
        error.setDescription(apiException.getDescription());
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setTimestamp(String.valueOf(Instant.now().toEpochMilli()));
        errorResponse.setErrors(Collections.singletonList(error));
        return errorResponse;
    }
}
