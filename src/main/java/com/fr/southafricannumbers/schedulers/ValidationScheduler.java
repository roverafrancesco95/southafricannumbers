package com.fr.southafricannumbers.schedulers;

import com.fr.southafricannumbers.api.models.SinglePhoneNumberResponse;
import com.fr.southafricannumbers.database.entities.FileUploaded;
import com.fr.southafricannumbers.database.entities.PhoneNumber;
import com.fr.southafricannumbers.database.enums.FileUploadedStatus;
import com.fr.southafricannumbers.database.enums.PhoneNumberStatusEnum;
import com.fr.southafricannumbers.services.FileUploadedService;
import com.fr.southafricannumbers.services.PhoneNumberService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class ValidationScheduler {
    @Value("${validator.max-fetchable-rows}")
    private int maxFetchableNumbers;

    private final FileUploadedService fileUploadedService;
    private final PhoneNumberService phoneNumberService;

    @Scheduled(cron = "${validator.cron-expression}")
    public void validatePhoneNumbers() {
        log.info("START validatePhoneNumbers");

        Optional<FileUploaded> fileOpt = fileUploadedService.findFileToBeValidated();
        if (fileOpt.isPresent()) {
            FileUploaded file = fileOpt.get();
            long pageCount = phoneNumberService.getPagesCountByFileUploaded(file, maxFetchableNumbers);
            for (int page = 0; page < pageCount; page++) {
                PageRequest pageable = PageRequest.of(0, maxFetchableNumbers, Sort.by("id").ascending());
                List<PhoneNumber> phoneNumbers = phoneNumberService.getPhoneNumbersReadyToBeValidated(file, pageable);
                for (PhoneNumber phoneNumber : phoneNumbers) {
                    try {
                        SinglePhoneNumberResponse singlePhoneNumberResponse = phoneNumberService.validate(phoneNumber.getPhoneNumber());
                        singlePhoneNumberResponse.setStatusDescr(PhoneNumberStatusEnum.fromValue(singlePhoneNumberResponse.getStatus()).toString());

                        phoneNumber.setStatus(singlePhoneNumberResponse.getStatus());
                        phoneNumber.setDescription(singlePhoneNumberResponse.getCorrectionDescription());
                        phoneNumber.setPhoneNumberCorrection(singlePhoneNumberResponse.getCorrectedPhoneNumber());

                    } catch (Exception e) {
                        log.error(String.format("Error on validating phoneNumber with id [%s]. ErrorMsg: [%s]",
                                phoneNumber.getId(), e.getMessage()));
                        phoneNumber.setStatus(PhoneNumberStatusEnum.ERROR.getValue());
                    }
                }
                phoneNumberService.saveAll(phoneNumbers);
            }
            file.setStatus(FileUploadedStatus.VALIDATED.getValue());
            fileUploadedService.save(file);
        } else {
            log.info("No file valid to be validated found");
        }
        log.info("END validatePhoneNumbers");
    }
}
