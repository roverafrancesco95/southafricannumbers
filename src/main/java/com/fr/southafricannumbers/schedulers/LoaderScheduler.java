package com.fr.southafricannumbers.schedulers;

import com.fr.southafricannumbers.database.entities.DiscardedRow;
import com.fr.southafricannumbers.database.entities.FileUploaded;
import com.fr.southafricannumbers.database.entities.PhoneNumber;
import com.fr.southafricannumbers.database.enums.FileUploadedStatus;
import com.fr.southafricannumbers.services.DiscardedRowService;
import com.fr.southafricannumbers.services.FileUploadedService;
import com.fr.southafricannumbers.services.PhoneNumberService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service
@RequiredArgsConstructor
public class LoaderScheduler {

    @Value("${dir.tmp}")
    private String tmpDir;
    @Value("${dir.dump}")
    private String dumpDir;
    @Value("${dir.processed}")
    private String processedDir;
    @Value("${loader.batch-size}")
    private int batchSize;

    private final FileUploadedService fileUploadedService;
    private final PhoneNumberService phoneNumberService;
    private final DiscardedRowService discardedRowService;

    @Scheduled(cron = "${loader.cron-expression}")
    public void loadPhoneNumbers() throws Exception {
        log.info("START loadPhoneNumbers");

        File folder = new File(tmpDir);
        if (!folder.exists() || !folder.isDirectory()) {
            throw new Exception(String.format("Specified dir doesn't exist or is not a dir [%s]", tmpDir));
        }

        for (File file : getAllFilesByDir(folder)) {
            boolean isValidFile = validateFile(file);
            FileUploaded fileUploaded = new FileUploaded(file.getName(), isValidFile);

            try {
                fileUploaded = fileUploadedService.save(fileUploaded);

                if (isValidFile) {
                    loadRows(file, fileUploaded);
                }
                moveFile(file, isValidFile);
                fileUploaded.setStatus(FileUploadedStatus.VALIDABLE.getValue());
            } catch (Exception e) {
                log.error(String.format("Error on loading file [%s]. ErrorMsg:", file.getName(), e.getMessage()));
                fileUploaded.setValid(false);
            }
            fileUploadedService.save(fileUploaded);
        }
        log.info("END loadPhoneNumbers");
    }

    private void loadRows(File file, FileUploaded fileUploaded) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file));

        String line;
        reader.readLine();
        List<PhoneNumber> phones = new ArrayList<>();
        List<DiscardedRow> discardedRows = new ArrayList<>();
        while ((line = reader.readLine()) != null) {
            try {
                String[] split = line.split(",");
                phones.add(new PhoneNumber(split[0], split[1], fileUploaded));

                if (phones.size() > batchSize)
                    saveBatchPhoneNumbers(phones);
            } catch (Exception e) {
                log.error(String.format("Error on loading row [%s]. %s", line, e.getMessage()));
                discardedRows.add(new DiscardedRow(line, fileUploaded));
                if (discardedRows.size() > batchSize)
                    saveDiscardedRows(discardedRows);
            }
        }
        saveBatchPhoneNumbers(phones);
        saveDiscardedRows(discardedRows);
        reader.close();
    }

    private void saveBatchPhoneNumbers(List<PhoneNumber> phones) {
        log.info("Saving phones number");
        phoneNumberService.saveAll(phones);
        phones.clear();
    }

    private void saveDiscardedRows(List<DiscardedRow> discardedRows) {
        log.info("Saving discarded rows");
        discardedRowService.saveAll(discardedRows);
        discardedRows.clear();
    }

    private void moveFile(File src, boolean isValidFile) throws IOException {
        String destDir = processedDir;
        if (!isValidFile)
            destDir = dumpDir;
        File destFile = new File(destDir + "/" + src.getName());
        Files.move(src.toPath(), destFile.toPath());
    }

    private List<File> getAllFilesByDir(File dir) {
        List<File> files = new ArrayList<>();
        for (File file : Objects.requireNonNull(dir.listFiles())) {
            if (file.isFile()) {
                files.add(file);
            }
        }
        return files;
    }

    private boolean validateFile(File file) {
        //TODO TO BE IMPLEMENTED - file validation
        return true;
    }
}
