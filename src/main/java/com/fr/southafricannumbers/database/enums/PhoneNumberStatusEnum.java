package com.fr.southafricannumbers.database.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum PhoneNumberStatusEnum {
    LOADED(0),
    VALID(1),
    CORRECTED(2),
    INVALID(3),
    ERROR(4);
    private final int value;

    public static PhoneNumberStatusEnum fromValue(int value) {
        for (PhoneNumberStatusEnum status : PhoneNumberStatusEnum.values()) {
            if (status.value == value) {
                return status;
            }
        }
        return ERROR;
    }
}

