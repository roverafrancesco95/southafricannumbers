package com.fr.southafricannumbers.database.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum FileUploadedStatus {
    LOADING(0),
    VALIDABLE(1),
    VALIDATED(2),
    ERROR(3);

    private final int value;
}
