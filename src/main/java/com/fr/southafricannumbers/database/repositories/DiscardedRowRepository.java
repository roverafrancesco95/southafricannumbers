package com.fr.southafricannumbers.database.repositories;

import com.fr.southafricannumbers.database.entities.DiscardedRow;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DiscardedRowRepository extends CrudRepository<DiscardedRow, Integer> {
}