package com.fr.southafricannumbers.database.repositories;

import com.fr.southafricannumbers.database.entities.FileUploaded;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FileUploadedRepository extends CrudRepository<FileUploaded, String> {
    Optional<FileUploaded> findFirstByStatusAndIsValidOrderByCreationTimestampAsc(int status, boolean valid);
}