package com.fr.southafricannumbers.database.repositories;

import com.fr.southafricannumbers.database.entities.FileUploaded;
import com.fr.southafricannumbers.database.entities.PhoneNumber;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PhoneNumberRepository extends CrudRepository<PhoneNumber, Integer> {
    List<PhoneNumber> findByFileUploaded(FileUploaded fileUploaded);
    List<PhoneNumber> findByStatusGreaterThan(int status, Pageable pageable);
    List<PhoneNumber> findByFileUploadedAndStatusOrderByIdAsc(FileUploaded fileUploaded, int status, Pageable pageable);
    long countByFileUploadedAndStatus(FileUploaded fileUploaded, int status);
}