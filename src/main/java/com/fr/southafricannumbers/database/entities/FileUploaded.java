package com.fr.southafricannumbers.database.entities;

import com.fr.southafricannumbers.database.enums.FileUploadedStatus;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "files_uploaded")
public class FileUploaded {
    @Id
    private String id;

    @Column(nullable = false)
    private boolean isValid;

    @Column(nullable = false)
    private int status;

    @CreationTimestamp
    private Timestamp creationTimestamp;

    public FileUploaded(String uuid, boolean isValid) {
        this.id = uuid;
        this.isValid = isValid;
        this.status = FileUploadedStatus.LOADING.getValue();
    }
}
