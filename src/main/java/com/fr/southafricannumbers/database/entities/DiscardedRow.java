package com.fr.southafricannumbers.database.entities;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "DISCARDED_ROWS")
public class DiscardedRow {
    @Id
    @SequenceGenerator(name = "discarded_rows_sequence", sequenceName = "discarded_rows_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "discarded_rows_sequence")
    private Integer id;

    @Column(nullable = false)
    private String rowValue;

    @CreationTimestamp
    private Timestamp creationTimestamp;

    @ToString.Exclude
    @ManyToOne
    private FileUploaded fileUploaded;

    public DiscardedRow(String rowValue, FileUploaded fileUploaded) {
        this.rowValue = rowValue;
        this.fileUploaded = fileUploaded;
    }
}
