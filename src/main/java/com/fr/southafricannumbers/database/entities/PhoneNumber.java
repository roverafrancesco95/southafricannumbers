package com.fr.southafricannumbers.database.entities;

import com.fr.southafricannumbers.database.enums.PhoneNumberStatusEnum;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "phone_numbers")
public class PhoneNumber {
    @Id
    @SequenceGenerator(name = "numbers_sequence", sequenceName = "numbers_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "numbers_sequence")
    private Integer id;
    private String phoneId;
    @Column(nullable = false)
    private String phoneNumber;

    @Column(nullable = false)
    private int status;

    private String phoneNumberCorrection;
    private String description;

    @CreationTimestamp
    private Timestamp creationTimestamp;

    @ToString.Exclude
    @ManyToOne
    private FileUploaded fileUploaded;

    public PhoneNumber(String phoneId, String phoneNumber, FileUploaded fileUploaded) {
        this.status = PhoneNumberStatusEnum.LOADED.getValue();
        this.phoneId = phoneId;
        this.phoneNumber = phoneNumber;
        this.fileUploaded = fileUploaded;
    }
}
